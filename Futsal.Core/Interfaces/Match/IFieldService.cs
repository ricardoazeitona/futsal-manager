﻿using Futsal.Core.Models.Match;
using System;
using System.Collections.Generic;
using System.Text;

namespace Futsal.Core.Interfaces.Match
{
    public interface IFieldService
    {
        void SetHomeTeam(List<PlayerMatch> players);
        void SetAwayTeam(List<PlayerMatch> players);
    }
}
