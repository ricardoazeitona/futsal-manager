﻿using Futsal.Core.Models.Match;
using System.Collections.Generic;
using System.Drawing;

namespace Futsal.Core.Interfaces.Match
{
    public interface IPlayerMatchService
    {
        bool HasBall();
        Point AskPoition(PlayerMatch player);
        List<FieldPoint> ValidatePosition(List<PlayerMatch> players);
    }
}
