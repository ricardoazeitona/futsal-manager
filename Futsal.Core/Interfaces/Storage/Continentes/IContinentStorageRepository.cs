﻿using Futsal.Core.Models;
using System.Collections.Generic;

namespace Futsal.Core.Interfaces.Storage.Continents
{
    public interface IContinentStorageRepository
    {
        List<Continent> GetContinents();
        bool Save(List<Continent> continents);
    }
}
