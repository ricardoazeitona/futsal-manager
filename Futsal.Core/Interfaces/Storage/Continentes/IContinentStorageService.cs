﻿using Futsal.Core.Models;
using System.Collections.Generic;

namespace Futsal.Core.Interfaces.Storage.Continents
{
    public interface IContinentStorageService
    {
        void Load(List<Continent> continents);
        List<Continent> Get();
        void AddBaseContinents();
    }
}
