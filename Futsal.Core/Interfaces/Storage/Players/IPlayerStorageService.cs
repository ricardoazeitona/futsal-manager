﻿using Futsal.Core.Models;
using System.Collections.Generic;

namespace Futsal.Core.Interfaces.Storage.Players
{
    public interface IPlayerStorageService
    {
        void Load(List<Player> players, List<Country> countries, List<Team> teams);
        List<Player> Get();
        void AddBasePlayers();
    }
}
