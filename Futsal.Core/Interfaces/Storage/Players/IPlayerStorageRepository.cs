﻿using Futsal.Core.Models;
using System.Collections.Generic;

namespace Futsal.Core.Interfaces.Storage.Players
{
    public interface IPlayerStorageRepository
    {
        List<Player> GetPlayers();
        bool Save(List<Player> players);
    }
}
