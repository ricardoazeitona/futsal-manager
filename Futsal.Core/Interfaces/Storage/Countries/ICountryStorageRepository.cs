﻿using Futsal.Core.Models;
using System.Collections.Generic;

namespace Futsal.Core.Interfaces.Storage.Countries
{
    public interface ICountryStorageRepository
    {
        List<Country> GetCountries();
        bool Save(List<Country> countries);
    }
}
