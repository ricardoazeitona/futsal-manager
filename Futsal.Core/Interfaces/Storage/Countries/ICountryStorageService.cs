﻿using Futsal.Core.Models;
using System.Collections.Generic;

namespace Futsal.Core.Interfaces.Storage.Countries
{
    public interface ICountryStorageService
    {
        void Load(List<Country> countries, List<Continent> continents);
        List<Country> Get();
        void AddBaseCountries();
    }
}
