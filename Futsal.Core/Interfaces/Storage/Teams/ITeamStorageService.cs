﻿using Futsal.Core.Models;
using System.Collections.Generic;

namespace Futsal.Core.Interfaces.Storage.Teams
{
    public interface ITeamStorageService
    {
        void Load(List<Team> teams, List<Country> countries);
        List<Team> Get();
        void AddBaseTeams();
    }
}
