﻿using Futsal.Core.Models;
using System.Collections.Generic;

namespace Futsal.Core.Interfaces.Storage.Teams
{
    public interface ITeamStorageRepository
    {
        List<Team> GetTeams();
        bool Save(List<Team> teams);
    }
}
