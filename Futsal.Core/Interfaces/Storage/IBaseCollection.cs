﻿namespace Futsal.Core.Interfaces.Storage
{
    public interface IBaseCollection
    {
        long Id { get; set; }
    }
}
