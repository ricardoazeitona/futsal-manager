﻿namespace Futsal.Core.Interfaces
{
    public interface IStorageService
    {
        void GenerateBaseData();
        void Save();
    }
}
