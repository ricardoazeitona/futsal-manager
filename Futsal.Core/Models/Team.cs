﻿using Futsal.Core.Interfaces.Storage;
using System.Collections.Generic;

namespace Futsal.Core.Models
{
    public class Team : IBaseCollection
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public Country Country { get; set; }
        public ICollection<Player> Players { get; set; }
        public ICollection<Competition> Competitions { get; set; }
    }
}