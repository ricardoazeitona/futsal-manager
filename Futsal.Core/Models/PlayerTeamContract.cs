﻿using Futsal.Core.Models.Attributes;
using System;

namespace Futsal.Core.Models
{
    public class PlayerTeamContract
    {
        public Team Team { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal WeekSalary { get; set; }
        public ContractType ContractType { get; set; }
    }
}
