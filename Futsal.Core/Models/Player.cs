﻿using Futsal.Core.Models.Attributes;
using System.Collections.Generic;

namespace Futsal.Core.Models
{
    public class Player : Person
    {
        public float Ability { get; set; }
        public float Potential { get; set; }
        public GoalkeeperAttribute GoalkeeperAttribute { get; set; }
        public MentalAttribute MentalAttribute { get; set; }
        public PhysicalAttribute PhysicalAttribute { get; set; }
        public TechnicalAttribute TechnicalAttribute { get; set; }
        public Dictionary<PositionEnum, float> Position { get; set; }
        public ICollection<PlayerTeamContract> Contract { get; set; }
    }
}
