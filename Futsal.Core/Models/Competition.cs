﻿using Futsal.Core.Interfaces.Storage;
using Futsal.Core.Models.Types;
using System.Collections.Generic;

namespace Futsal.Core.Models
{
    public class Competition : IBaseCollection
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public LeagueType Type { get; set; }
        public int Tier { get; set; }
        public Country Country { get; set; }
        public List<Team> Teams { get; set; }
    }
}
