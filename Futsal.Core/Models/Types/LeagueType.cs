﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Futsal.Core.Models.Types
{
    public enum LeagueType
    {
        League,
        Cup
    }
}
