﻿using Futsal.Core.Interfaces.Storage;

namespace Futsal.Core.Models
{
    public class Country : IBaseCollection
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ThreeLetter { get; set; }
        public Continent Continent { get; set; }
        public decimal Reputation { get; set; }
    }
}
