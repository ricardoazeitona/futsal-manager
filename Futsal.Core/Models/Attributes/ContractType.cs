﻿namespace Futsal.Core.Models.Attributes
{
    public enum ContractType
    {
        Full,
        Loan
    }
}
