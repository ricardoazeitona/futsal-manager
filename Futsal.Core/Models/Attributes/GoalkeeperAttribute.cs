﻿namespace Futsal.Core.Models.Attributes
{
    public class GoalkeeperAttribute
    {
        public float Communication { get; set; }
        public float Hands { get; set; }
        public float Reflexs { get; set; }
        public float Pass { get; set; }
        public float FirstTouch { get; set; }
        public float OneToOne { get; set; }
    }
}
