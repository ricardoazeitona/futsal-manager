﻿namespace Futsal.Core.Models.Attributes
{
    public class TechnicalAttribute
    {
        public float Tackle { get; set; }
        public float Finish { get; set; }
        public float Dribble { get; set; }
        public float Marking { get; set; }
        public float Penalty { get; set; }
        public float Pass { get; set; }
        public float FirstTouch { get; set; }
        public float Shot { get; set; }
        public float Technique { get; set; }
        public float BallControl { get; set; }
    }
}
