﻿namespace Futsal.Core.Models.Attributes
{
    public enum PositionEnum
    {
        Goalkeeper,
        Defender,
        Winger,
        Pivot,
    }
}
