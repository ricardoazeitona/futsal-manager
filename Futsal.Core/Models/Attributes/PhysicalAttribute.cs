﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Futsal.Core.Models.Attributes
{
    public class PhysicalAttribute
    {
        public float Aceleration { get; set; }
        public float Agility { get; set; }
        public float PhysicalCondition { get; set; }
        public float Strength { get; set; }
        public float Resistence { get; set; }
        public float Recover { get; set; }
    }
}
