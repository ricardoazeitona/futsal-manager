﻿namespace Futsal.Core.Models.Attributes
{
    public class MentalAttribute
    {
        public float Aggressive { get; set; }
        public float Antecipation { get; set; }
        public float Concentration { get; set; }
        public float Decisions { get; set; }
        public float Determination { get; set; }
        public float WorkIndex { get; set; }
        public float Leadership { get; set; }
        public float Positioning { get; set; }
        public float WithoutBall { get; set; }
        public float WorkingRate { get; set; }
        public float Vision { get; set; }
    }
}
