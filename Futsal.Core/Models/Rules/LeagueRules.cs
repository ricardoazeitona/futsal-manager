﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Futsal.Core.Models.Rules
{
    public class LeagueRules
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int MyProperty { get; set; }

    }
}
