﻿using Futsal.Core.Models.Match;
using System.Collections.Generic;

namespace Futsal.Core.Models.Game
{
    public class PlayerStats
    {
        public long PlayerId { get; set; }
        public int Games { get; set; }
        public int Goals { get; set; }
        public int Assists { get; set; }
        public int YellowCards { get; set; }
        public int RedCards { get; set; }
        public int OwnGoals { get; set; }
        public int Starting { get; set; }
        public int Bench { get; set; }
        public int Saves { get; set; }
        public int GoalsAllowed { get; set; }
        public List<PlayerMatchStats> GameStats { get; set; } = new List<PlayerMatchStats>();

    }
}
