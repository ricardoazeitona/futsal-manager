﻿using System;
using System.Collections.Generic;

namespace Futsal.Core.Models.Game
{
    public class GameInfo
    {
        public DateTime Date { get; set; }
        public long ManagerId { get; set; }
        public long TeamId { get; set; }
        public ICollection<Mail> Mail { get; set; }

    }
}
