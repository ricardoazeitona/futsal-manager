﻿namespace Futsal.Core.Models.Game
{
    public enum MailType
    {
        Player,
        Team,
        League
    }
}
