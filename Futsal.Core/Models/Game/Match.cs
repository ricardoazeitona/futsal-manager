﻿using Futsal.Core.Interfaces.Storage;
using Futsal.Core.Models;
using System;

namespace Futsal.Core.Game
{
    public class Match : IBaseCollection
    {
        public long Id { get; set; }
        public Competition Competition { get; set; }
        public Team HomeTeam { get; set; }
        public Team AwayTeam { get; set; }
        public DateTime Date { get; set; }
    }
}
