﻿using Futsal.Core.Interfaces.Storage;

namespace Futsal.Core.Models.Game
{
    public class Mail : IBaseCollection
    {
        public long Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public MailType Type { get; set; }
        public bool IsRead { get; set; }
    }
}
