﻿using Futsal.Core.Interfaces.Storage;
using System;

namespace Futsal.Core.Models
{
    public class Person : IBaseCollection
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public Country Country { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
    }
}
