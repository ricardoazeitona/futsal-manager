﻿using Futsal.Core.Interfaces.Storage;

namespace Futsal.Core.Models
{
    public class Continent : IBaseCollection
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
