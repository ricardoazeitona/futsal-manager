﻿using System.Collections.Generic;

namespace Futsal.Core.Models.Match
{
    public class Field
    {
        public Ball Ball { get; set; }
        public List<PlayerMatch> HomePlayer { get; set; }
        public List<PlayerMatch> AwayPlayer { get; set; }
    }
}
