﻿using System.Drawing;

namespace Futsal.Core.Models.Match
{
    public class FieldElement
    {
        public Point Position { get; set; }
    }
}
