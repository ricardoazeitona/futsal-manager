﻿namespace Futsal.Core.Models.Match.Instructions
{
    public enum PlayerMatchMarkingType
    {
        Zone,
        ManToMan,
    }
}
