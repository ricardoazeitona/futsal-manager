﻿namespace Futsal.Core.Models.Match.Instructions
{
    public enum PlayerFieldPositionType
    {
        Goalkeeper,
        Defender,
        Midfielder,
        Forward
    }
}
