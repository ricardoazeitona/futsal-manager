﻿using Futsal.Core.Models.Match.Instructions;

namespace Futsal.Core.Models.Match.Instructions
{
    public class PlayerMatchInstructions
    {
        public PlayerFieldPositionType StartPosition { get; set; }
        public float Mentality { get; set; }
        public float Pass { get; set; }
        public float Pressure { get; set; }
        public float Marking { get; set; }
        public PlayerMatchMarkingType MarkingType { get; set; }
        public PlayerMatch MarkingPlayer { get; set; }
        public float Tackle { get; set; }
    }
}
