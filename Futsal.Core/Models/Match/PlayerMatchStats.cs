﻿namespace Futsal.Core.Models.Match
{
    public class PlayerMatchStats
    {
        public bool Starting { get; set; }
        public int MinutesPlayed { get; set; }
        public int Goals { get; set; }
        public int Assists { get; set; }
        public int Passes { get; set; }
        public int PassesCompleted { get; set; }
        public int Tackles { get; set; }
        public int TacklesCompleted { get; set; }
        public int YellowCards { get; set; }
        public int RedCards { get; set; }
        public int OwnGoals { get; set; }
        public int Saves { get; set; }
        public int GoalsAllowed { get; set; }
    }
}
