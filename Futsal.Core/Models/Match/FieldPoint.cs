﻿using System.Drawing;

namespace Futsal.Core.Models.Match
{
    public class FieldPoint
    {
        public Point Point { get; set; }
        public FieldElement Player { get; set; }
    }
}
