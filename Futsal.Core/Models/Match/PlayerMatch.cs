﻿using Futsal.Core.Models.Match.Instructions;

namespace Futsal.Core.Models.Match
{
    public class PlayerMatch : FieldElement
    {
        public Player Player { get; set; }
        public bool InField { get; set; }
        public bool HasBall { get; set; }
        public PlayerMatchInstructions PlayerInstructions { get; set; }
        public TeamMatchInstructions TeamInstructions { get; set; }
        public PlayerMatchStats Stats { get; set; }
    }
}
