﻿using Futsal.Core.Interfaces;
using Futsal.Core.Interfaces.Storage.Continents;
using Futsal.Core.Interfaces.Storage.Countries;
using Futsal.Core.Interfaces.Storage.Players;
using Futsal.Core.Interfaces.Storage.Teams;
using Futsal.Core.Models;
using LiteDB;

namespace Futsal.Repository
{
    public class StorageService : IStorageService
    {
        private readonly IContinentStorageRepository _continentRepository;
        private readonly ICountryStorageRepository _countryRepository;
        private readonly ITeamStorageRepository _teamRepository;
        private readonly IPlayerStorageRepository _playerRepository;

        private readonly IContinentStorageService _continentService;
        private readonly ICountryStorageService _countryService;
        private readonly ITeamStorageService _teamService;
        private readonly IPlayerStorageService _playerService;

        public StorageService(IContinentStorageRepository continentRepository, ICountryStorageRepository countryRepository, ITeamStorageRepository teamRepository,
            IPlayerStorageRepository playerRepository,
            IContinentStorageService continentService, ICountryStorageService countryService, ITeamStorageService teamService,
            IPlayerStorageService playerService
            )
        {
            // Re-use mapper from global instance
            var mapper = BsonMapper.Global;

            // Produts and Customer are other collections (not embedded document)
            // you can use [BsonRef("colname")] attribute
            mapper.Entity<Country>()
                .DbRef(x => x.Continent, "continent");

            mapper.Entity<Team>()
                .DbRef(x => x.Country, "country");

            mapper.Entity<Player>()
                .DbRef(x => x.Country, "country");

            mapper.Entity<Team>()
                .DbRef(x => x.Players, "player");


            _continentRepository = continentRepository;
            _countryRepository = countryRepository;
            _teamRepository = teamRepository;
            _playerRepository = playerRepository;

            _continentService = continentService;
            _countryService = countryService;
            _teamService = teamService;
            _playerService = playerService;

            _continentService.Load(_continentRepository.GetContinents());
            _countryService.Load(_countryRepository.GetCountries(), _continentService.Get());
            _teamService.Load(_teamRepository.GetTeams(), _countryService.Get());
            _playerService.Load(_playerRepository.GetPlayers(), _countryService.Get(), _teamService.Get());
        }

        public void GenerateBaseData()
        {
            _continentService.AddBaseContinents();
            _countryService.AddBaseCountries();
            _teamService.AddBaseTeams();
            _playerService.AddBasePlayers();
        }

        public void Save()
        {
            _continentRepository.Save(_continentService.Get());
            _countryRepository.Save(_countryService.Get());
            _teamRepository.Save(_teamService.Get());
            _playerRepository.Save(_playerService.Get());
        }
    }
}
