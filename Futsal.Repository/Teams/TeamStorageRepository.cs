﻿using Futsal.Core.Interfaces.Storage.Teams;
using Futsal.Core.Models;
using System.Collections.Generic;
using System.Linq;

namespace Futsal.Repository.Teams
{
    public class TeamStorageRepository : ITeamStorageRepository
    {
        private readonly StorageRepository<Team> _repository;

        public TeamStorageRepository(StorageRepository<Team> repository)
        {
            _repository = repository;
        }

        public List<Team> GetTeams()
        {
            return _repository.List().ToList();
        }

        public bool Save(List<Team> teams)
        {
            return _repository.Save(teams);
        }
    }
}
