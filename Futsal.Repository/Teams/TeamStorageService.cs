﻿using Futsal.Core.Interfaces.Storage.Teams;
using Futsal.Core.Models;
using Futsal.Repository.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace Futsal.Repository.Teams
{
    public class TeamStorageService : ITeamStorageService
    {
        private List<Team> _teams;
        private List<Country> _countries;

        public void Load(List<Team> teams, List<Country> countries)
        {
            _teams = teams;
            _countries = countries;
        }

        public List<Team> Get() => _teams;

        public void AddBaseTeams()
        {
            var countryPortugal = _countries.FirstOrDefault(q => q.Name == "Portugal");

            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "Beleneses",
                Country = countryPortugal,
                Players = new List<Player>()
            });
            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "Benfica",
                Country = countryPortugal,
                Players = new List<Player>()
            });
            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "SC Braga/AAUM",
                Country = countryPortugal,
                Players = new List<Player>()
            });
            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "Burinhosa",
                Country = countryPortugal,
                Players = new List<Player>()
            });
            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "Desportivo Aves",
                Country = countryPortugal,
                Players = new List<Player>()
            });
            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "Fabril Barreiro",
                Country = countryPortugal,
                Players = new List<Player>()
            });
            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "Fundão",
                Country = countryPortugal,
                Players = new List<Player>()
            });
            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "Futsal Azeméis",
                Country = countryPortugal,
                Players = new List<Player>()
            });
            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "Leões de Porto Salvo",
                Country = countryPortugal,
                Players = new List<Player>()
            });
            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "Modicus",
                Country = countryPortugal,
                Players = new List<Player>()
            });
            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "Pinheirense",
                Country = countryPortugal,
                Players = new List<Player>()
            });
            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "Quinta dos Lobos",
                Country = countryPortugal,
                Players = new List<Player>()
            });
            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "Sporting CP",
                Country = countryPortugal,
                Players = new List<Player>()
            });
            _teams.Add(new Team()
            {
                Id = _teams.NewId(),
                Name = "Rio Ave",
                Country = countryPortugal,
                Players = new List<Player>()
            });
        }
    }
}
