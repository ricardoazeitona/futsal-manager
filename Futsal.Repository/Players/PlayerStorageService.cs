﻿using Bogus;
using Bogus.DataSets;
using Futsal.Core.Interfaces.Storage.Players;
using Futsal.Core.Models;
using Futsal.Core.Models.Attributes;
using Futsal.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Futsal.Repository.Players
{
    public class PlayerStorageService : IPlayerStorageService
    {
        private List<Player> _players;
        private List<Country> _countries;
        private List<Team> _teams;

        public void Load(List<Player> players, List<Country> countries, List<Team> teams)
        {
            _players = players;
            _countries = countries;
            _teams = teams;
        }

        public List<Player> Get() => _players;

        public void AddBasePlayers()
        {
            var countryPortugal = _countries.FirstOrDefault(q => q.Name == "Portugal");

            for (int i = 0; i < (8 * 14); i++)
            {
                _players.Add(new Faker<Player>()
                    .StrictMode(false)
                    .RuleFor(p => p.Id, _players.NewId())
                    .RuleFor(p => p.FirstName, v => v.Name.FirstName(Name.Gender.Male))
                    .RuleFor(p => p.LastName, v => v.Name.LastName(Name.Gender.Male))
                    .RuleFor(p => p.BirthDate, v => v.Date.Between(new DateTime(1980, 1, 1), new DateTime(2000, 12, 31)))
                    .RuleFor(p => p.Height, v => v.Random.Int(158, 210))
                    .RuleFor(p => p.Weight, v => v.Random.Int(57, 100))
                    .RuleFor(p => p.Country, v => countryPortugal)
                    .RuleFor(p => p.GoalkeeperAttribute, v => new Faker<GoalkeeperAttribute>()
                                .Rules((g, vg) =>
                                {
                                    vg.Communication = g.Random.Float(0, 1) * 5;
                                    vg.FirstTouch = g.Random.Float(0, 1) * 5;
                                    vg.Hands = g.Random.Float(0, 1) * 5;
                                    vg.OneToOne = g.Random.Float(0, 1) * 5;
                                    vg.Pass = g.Random.Float(0, 1) * 5;
                                    vg.Reflexs = g.Random.Float(0, 1) * 5;
                                }).Generate())
                    .RuleFor(p => p.MentalAttribute, v => new Faker<MentalAttribute>()
                                .Rules((m, vm) =>
                                {
                                    vm.Aggressive = m.Random.Float(0, 1) * 20;
                                    vm.Antecipation = m.Random.Float(0, 1) * 20;
                                    vm.Concentration = m.Random.Float(0, 1) * 20;
                                    vm.Decisions = m.Random.Float(0, 1) * 20;
                                    vm.Determination = m.Random.Float(0, 1) * 20;
                                    vm.Leadership = m.Random.Float(0, 1) * 20;
                                    vm.Positioning = m.Random.Float(0, 1) * 20;
                                    vm.Vision = m.Random.Float(0, 1) * 20;
                                    vm.WithoutBall = m.Random.Float(0, 1) * 20;
                                    vm.WorkIndex = m.Random.Float(0, 1) * 20;
                                    vm.WorkingRate = m.Random.Float(0, 1) * 20;
                                }).Generate())
                    .RuleFor(p => p.PhysicalAttribute, v => new Faker<PhysicalAttribute>()
                                .Rules((g, vg) =>
                                {
                                    vg.Aceleration = g.Random.Float(0, 1) * 20;
                                    vg.Agility = g.Random.Float(0, 1) * 20;
                                    vg.PhysicalCondition = g.Random.Float(0, 1) * 20;
                                    vg.Recover = g.Random.Float(0, 1) * 20;
                                    vg.Resistence = g.Random.Float(0, 1) * 20;
                                    vg.Strength = g.Random.Float(0, 1) * 20;
                                }).Generate())
                    .RuleFor(p => p.TechnicalAttribute, v => new Faker<TechnicalAttribute>()
                                .Rules((g, vg) =>
                                {
                                    vg.Dribble = g.Random.Float(0, 1) * 20;
                                    vg.Finish = g.Random.Float(0, 1) * 20;
                                    vg.FirstTouch = g.Random.Float(0, 1) * 20;
                                    vg.Marking = g.Random.Float(0, 1) * 20;
                                    vg.Pass = g.Random.Float(0, 1) * 20;
                                    vg.Penalty = g.Random.Float(0, 1) * 20;
                                    vg.Shot = g.Random.Float(0, 1) * 20;
                                    vg.Tackle = g.Random.Float(0, 1) * 20;
                                    vg.Technique = g.Random.Float(0, 1) * 20;
                                    vg.BallControl = g.Random.Float(0, 1) * 20;
                                }).Generate())
                    .RuleFor(p => p.Contract, v => new Faker<ICollection<PlayerTeamContract>>()
                        .Rules((ptc, pv) =>
                        {
                            pv.Add(new Faker<PlayerTeamContract>()
                                .RuleFor(t => t.Team, tv => tv.PickRandom<Team>(_teams.Where(q => q.Players.Count < 8)))
                                .RuleFor(t => t.StartDate, tv => tv.Date.Between(new DateTime(2010, 1, 1), new DateTime(2016, 12, 31)))
                                .RuleFor(t => t.EndDate, tv => tv.Date.Between(new DateTime(2020, 1, 1), new DateTime(2025, 12, 31)))
                                .RuleFor(t => t.WeekSalary, tv => tv.Random.Decimal(66, 4000))
                                .Generate());
                        })
                        .Generate()
                    )
                    .RuleFor(p => p.Position, v => new Dictionary<PositionEnum, float>() { { v.PickRandom(PositionEnum.Defender, PositionEnum.Pivot, PositionEnum.Winger), (v.Random.Float(0, 1) * 20) } })
                    .Generate()
                );

                var team = _teams.FirstOrDefault(q => q.Id == _players.Last().Contract.LastOrDefault().Team.Id);
                team.Players.Add(_players.Last());
            }


            for (int i = 0; i < (2 * 14); i++)
            {
                _players.Add(new Faker<Player>()
                    .StrictMode(false)
                    .RuleFor(p => p.Id, _players.NewId())
                    .RuleFor(p => p.FirstName, v => v.Name.FirstName(Name.Gender.Male))
                    .RuleFor(p => p.LastName, v => v.Name.LastName(Name.Gender.Male))
                    .RuleFor(p => p.BirthDate, v => v.Date.Between(new DateTime(1980, 1, 1), new DateTime(2000, 12, 31)))
                    .RuleFor(p => p.Height, v => v.Random.Int(158, 210))
                    .RuleFor(p => p.Weight, v => v.Random.Int(57, 100))
                    .RuleFor(p => p.Country, v => countryPortugal)
                    .RuleFor(p => p.GoalkeeperAttribute, v => new Faker<GoalkeeperAttribute>()
                                .Rules((g, vg) =>
                                {
                                    vg.Communication = g.Random.Float(0, 1) * 20;
                                    vg.FirstTouch = g.Random.Float(0, 1) * 20;
                                    vg.Hands = g.Random.Float(0, 1) * 20;
                                    vg.OneToOne = g.Random.Float(0, 1) * 20;
                                    vg.Pass = g.Random.Float(0, 1) * 20;
                                    vg.Reflexs = g.Random.Float(0, 1) * 20;
                                }).Generate())
                    .RuleFor(p => p.MentalAttribute, v => new Faker<MentalAttribute>()
                                .Rules((m, vm) =>
                                {
                                    vm.Aggressive = m.Random.Float(0, 1) * 20;
                                    vm.Antecipation = m.Random.Float(0, 1) * 20;
                                    vm.Concentration = m.Random.Float(0, 1) * 20;
                                    vm.Decisions = m.Random.Float(0, 1) * 20;
                                    vm.Determination = m.Random.Float(0, 1) * 20;
                                    vm.Leadership = m.Random.Float(0, 1) * 20;
                                    vm.Positioning = m.Random.Float(0, 1) * 20;
                                    vm.Vision = m.Random.Float(0, 1) * 20;
                                    vm.WithoutBall = m.Random.Float(0, 1) * 20;
                                    vm.WorkIndex = m.Random.Float(0, 1) * 20;
                                    vm.WorkingRate = m.Random.Float(0, 1) * 20;
                                }).Generate())
                    .RuleFor(p => p.PhysicalAttribute, v => new Faker<PhysicalAttribute>()
                                .Rules((g, vg) =>
                                {
                                    vg.Aceleration = g.Random.Float(0, 1) * 20;
                                    vg.Agility = g.Random.Float(0, 1) * 20;
                                    vg.PhysicalCondition = g.Random.Float(0, 1) * 20;
                                    vg.Recover = g.Random.Float(0, 1) * 20;
                                    vg.Resistence = g.Random.Float(0, 1) * 20;
                                    vg.Strength = g.Random.Float(0, 1) * 20;
                                }).Generate())
                    .RuleFor(p => p.TechnicalAttribute, v => new Faker<TechnicalAttribute>()
                                .Rules((g, vg) =>
                                {
                                    vg.Dribble = g.Random.Float(0, 1) * 20;
                                    vg.Finish = g.Random.Float(0, 1) * 20;
                                    vg.FirstTouch = g.Random.Float(0, 1) * 20;
                                    vg.Marking = g.Random.Float(0, 1) * 20;
                                    vg.Pass = g.Random.Float(0, 1) * 20;
                                    vg.Penalty = g.Random.Float(0, 1) * 20;
                                    vg.Shot = g.Random.Float(0, 1) * 20;
                                    vg.Tackle = g.Random.Float(0, 1) * 20;
                                    vg.Technique = g.Random.Float(0, 1) * 20;
                                }).Generate())
                    .RuleFor(p => p.Contract, v => new Faker<ICollection<PlayerTeamContract>>()
                        .Rules((ptc, pv) =>
                        {
                            pv.Add(new Faker<PlayerTeamContract>()
                                .RuleFor(t => t.Team, tv => tv.PickRandom<Team>(_teams.Where(q => q.Players.Count < 10)))
                                .RuleFor(t => t.StartDate, tv => tv.Date.Between(new DateTime(2010, 1, 1), new DateTime(2016, 12, 31)))
                                .RuleFor(t => t.EndDate, tv => tv.Date.Between(new DateTime(2020, 1, 1), new DateTime(2025, 12, 31)))
                                .RuleFor(t => t.WeekSalary, tv => tv.Random.Decimal(66, 4000))
                                .Generate());
                        })
                        .Generate()
                    )


                    .RuleFor(p => p.Position, v => new Dictionary<PositionEnum, float>() { { PositionEnum.Goalkeeper, (v.Random.Float(0, 1) * 20) } })
                    .Generate()
                );

                var team = _teams.FirstOrDefault(q => q.Id == _players.Last().Contract.Last().Team.Id);
                team.Players.Add(_players.Last());
            }



        }
    }
}
