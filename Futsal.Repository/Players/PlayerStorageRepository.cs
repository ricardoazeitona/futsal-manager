﻿using Futsal.Core.Interfaces.Storage.Players;
using Futsal.Core.Models;
using System.Collections.Generic;
using System.Linq;

namespace Futsal.Repository.Players
{
    public class PlayerStorageRepository : IPlayerStorageRepository
    {
        private readonly StorageRepository<Player> _repository;

        public PlayerStorageRepository(StorageRepository<Player> repository)
        {
            _repository = repository;
        }

        public List<Player> GetPlayers()
        {
            return _repository.List().ToList();
        }

        public bool Save(List<Player> players)
        {
            return _repository.Save(players);
        }
    }
}
