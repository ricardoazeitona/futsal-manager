﻿using Futsal.Core.Interfaces.Storage.Countries;
using Futsal.Core.Models;
using System.Collections.Generic;

namespace Futsal.Repository.Countries
{
    public class CountryStorageRepositoy : ICountryStorageRepository
    {
        private readonly StorageRepository<Country> _repository;

        public CountryStorageRepositoy(StorageRepository<Country> repository)
        {
            _repository = repository;
        }

        public List<Country> GetCountries()
        {
            return _repository.List();
        }

        public bool Save(List<Country> countries)
        {
            return _repository.Save(countries);
        }
    }
}
