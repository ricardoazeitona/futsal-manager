﻿using Futsal.Core.Interfaces.Storage.Countries;
using Futsal.Core.Models;
using Futsal.Repository.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace Futsal.Repository.Countries
{
    public class CountryStorageService : ICountryStorageService
    {
        private List<Country> _countries;
        private List<Continent> _continents;

        public void Load(List<Country> countries, List<Continent> continents)
        {
            _countries = countries;
            _continents = continents;
        }

        public List<Country> Get() => _countries;

        public void AddBaseCountries()
        {
            var continent = _continents.FirstOrDefault(q => q.Name == "Europe");

            _countries.Add(new Country()
            {
                Id = _countries.NewId(),
                Name = "Portugal",
                ThreeLetter = "POR",
                Reputation = 180,
                Continent = continent
            });
        }
    }
}
