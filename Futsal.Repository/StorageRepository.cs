﻿using Futsal.Core.Interfaces.Storage;
using LiteDB;
using System.Collections.Generic;
using System.Linq;

namespace Futsal.Repository
{
    public class StorageRepository<T> where T : IBaseCollection
    {
        public List<T> List()
        {
            using (var db = new LiteDatabase($"Data/{typeof(T).Name}.db"))
            {
                var items = db.GetCollection<T>();
                return items.FindAll().ToList();
            }
        }

        public bool Save(List<T> items)
        {
            using (var db = new LiteDatabase($"Data/{typeof(T).Name}.db"))
            {
                var collection = db.GetCollection<T>();
                return collection.InsertBulk(items) == 1;
            }
        }
    }
}
