﻿using Futsal.Core.Interfaces.Storage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Futsal.Repository.Helpers
{
    public static class StorageExtension
    {
        public static int NewId<T>(this List<T> list) where T : IBaseCollection
        {
            return list.Count + 1;
        }

        private static Random random = new Random();
        public static string RandomString(int length = 8)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
