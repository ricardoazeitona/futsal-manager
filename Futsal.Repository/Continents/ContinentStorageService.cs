﻿using Futsal.Core.Interfaces.Storage.Continents;
using Futsal.Core.Models;
using Futsal.Repository.Helpers;
using System.Collections.Generic;

namespace Futsal.Repository.Continents
{
    public class ContinentStorageService : IContinentStorageService
    {
        private List<Continent> _continents;

        public void Load(List<Continent> continents)
        {
            _continents = continents;
        }

        public List<Continent> Get() => _continents;

        public void AddBaseContinents()
        {
            _continents.Add(new Continent()
            {
                Id = _continents.NewId(),
                Name = "Europe"
            });
            _continents.Add(new Continent()
            {
                Id = _continents.NewId(),
                Name = "Africa"
            });
            _continents.Add(new Continent()
            {
                Id = _continents.NewId(),
                Name = "Asia"
            });
            _continents.Add(new Continent()
            {
                Id = _continents.NewId(),
                Name = "Oceania"
            });
            _continents.Add(new Continent()
            {
                Id = _continents.NewId(),
                Name = "North America"
            });
            _continents.Add(new Continent()
            {
                Id = _continents.NewId(),
                Name = "South America"
            });
        }

    }
}
