﻿using Futsal.Core.Interfaces.Storage.Continents;
using Futsal.Core.Models;
using System.Collections.Generic;

namespace Futsal.Repository.Continents
{
    public class ContinentStorageRepository : IContinentStorageRepository
    {
        private readonly StorageRepository<Continent> _repository;

        public ContinentStorageRepository(StorageRepository<Continent> repository)
        {
            _repository = repository;
        }

        public List<Continent> GetContinents()
        {
            return _repository.List();
        }

        public bool Save(List<Continent> continents)
        {
            return _repository.Save(continents);
        }

    }
}
