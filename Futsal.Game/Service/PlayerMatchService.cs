﻿using Futsal.Core.Interfaces.Match;
using Futsal.Core.Models.Match;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Futsal.Game.Service
{
    public class PlayerMatchService : IPlayerMatchService
    {
        private PlayerMatch _player { get; set; }
        private List<PlayerMatch> _teamPlayer { get; set; }
        private List<PlayerMatch> _oppositePlayer { get; set; }

        public Point AskPoition(PlayerMatch player)
        {
            throw new System.NotImplementedException();
        }

        public bool HasBall()
        {
            return _player.HasBall;
        }

        public List<FieldPoint> ValidatePosition(List<PlayerMatch> players)
        {
            return players.Where(q => q.InField).Select(q => new FieldPoint() { Player = q, Point = q.Position }).ToList();
        }
    }
}
