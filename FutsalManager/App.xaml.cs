﻿using Futsal.Core.Interfaces;
using Futsal.Core.Interfaces.Storage.Continents;
using Futsal.Core.Interfaces.Storage.Countries;
using Futsal.Core.Interfaces.Storage.Players;
using Futsal.Core.Interfaces.Storage.Teams;
using Futsal.Repository;
using Futsal.Repository.Continents;
using Futsal.Repository.Countries;
using Futsal.Repository.Players;
using Futsal.Repository.Teams;
using System.Windows;
using Unity;

namespace FutsalManager
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            IUnityContainer container = new UnityContainer();
            container.RegisterType(typeof(StorageRepository<>), typeof(StorageRepository<>));

            container.RegisterType<IContinentStorageRepository, ContinentStorageRepository>();
            container.RegisterType<ICountryStorageRepository, CountryStorageRepositoy>();
            container.RegisterType<ITeamStorageRepository, TeamStorageRepository>();
            container.RegisterType<IPlayerStorageRepository, PlayerStorageRepository>();

            container.RegisterType<IStorageService, StorageService>();

            container.RegisterType<IContinentStorageService, ContinentStorageService>();
            container.RegisterType<ICountryStorageService, CountryStorageService>();
            container.RegisterType<ITeamStorageService, TeamStorageService>();
            container.RegisterType<IPlayerStorageService, PlayerStorageService>();

            MainWindow mainWindow = container.Resolve<MainWindow>();
            mainWindow.Show();
        }
    }
}
