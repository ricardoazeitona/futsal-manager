﻿using Futsal.Core.Interfaces;
using System.Windows;
using Unity.Attributes;

namespace FutsalManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [Dependency]
        public MainWindowViewModel ViewModel
        {
            set { DataContext = value; }
        }

        public MainWindow()
        {
            InitializeComponent();
        }
    }

    public class MainWindowViewModel
    {
        private readonly IStorageService _myData;
        public MainWindowViewModel(IStorageService myData)
        {
            _myData = myData;
            _myData.GenerateBaseData();
            _myData.Save();
        }
    }
}
